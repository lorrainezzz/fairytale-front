describe('Author list', function() {
  it('Read the author list', function() {
    cy.setCookie('name', 'caroline')
    cy.visit('/#/authors')

  })
  it('Add a new author', function(){
    cy.setCookie('name', 'caroline')
    cy.visit('/#/authors')
    cy.get(':nth-child(1) > :nth-child(5) > .fa').click()
  })
  it('Remove an author', function(){

    cy.setCookie('name', 'caroline')
    cy.visit('/#/authors')
    cy.get(':nth-child(3) > :nth-child(6) > .fa').click()

    cy.get('.swal2-confirm').click()
  })
  it('Filter by author name', function(){
    cy.setCookie('name', 'caroline')
    cy.visit('/#/fairytale')
    cy.get('.VueTables__author-filter-wrapper > .form-control').type('Oui')
      .should('have.value', 'Oui')
  })
  it('Filter by region', function(){
    cy.setCookie('name', 'caroline')
    cy.visit('/#/fairytale')
    cy.get('.VueTables__category-filter-wrapper > .form-control').type('Canadian')
      .should('have.value', 'Canadian')
  })

})
