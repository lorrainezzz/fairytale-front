# Assignment 2 - Agile Software Practice.

Name: Yang Zou
Student ID: 20086439

## Client UI.

>>Main page
![image](img/mainpage.jpg.png)

>> Allow user to register with a new account
![image](img/Register.png)

>>Allow user to sign in
![image](img/Sign in-1.png)
![image](img/Sign in-2.png)

>>Allow user to reset the password
![image](img/Reset pwd.png)
![image](img/Reset pwd-1.png)

>>Allow user to see the fairy tale list and like and delete the fairy tale
![image](img/fairytalelist-1.png)
![image](img/fairytalelist-2.png)

>>Allow user to see the author list and add and delete the fairy tale
![image](img/fairytalelist-3.png)


>>Allow user to add a new author
![image](img/addAuthor-1.png)

>>Allow user to add a new fairy tale
![image](img/addFairytale-1.png)

